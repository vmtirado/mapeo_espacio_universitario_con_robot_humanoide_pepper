#!/usr/bin/env python

import rospy 

from robot_toolkit_msgs.srv import vision_tools_srv

from robot_toolkit_msgs.msg import vision_tools_msg
from robot_toolkit_msgs.msg import camera_parameters_msg


def main():

	rospy.init_node('sIA_depth_camera_handler')

	rospy.wait_for_service('/robot_toolkit/vision_tools_srv')

	camParameters = camera_parameters_msg()
	#camParameters.fps = 1
	#camParameters.resolution = 1

	visionMsg = vision_tools_msg()
	visionMsg.camera_name = 'depth_camera'
	visionMsg.command = 'enable'
	visionMsg.frame_rate = 1
	#visionMsg.command = 'set_parameters'
	#visionMsg.camera_parameters = camParameters

	camera_srv = rospy.ServiceProxy('/robot_toolkit/vision_tools_srv', vision_tools_srv)

	resp = camera_srv(visionMsg)

	print(resp)

	camParameters = camera_parameters_msg()
	camParameters.compress = True
	#camParameters.compression_factor = 50
	camParameters.resolution = 3
	camParameters.fps = 5

	visionMsg.camera_name = 'front_camera'
	visionMsg.command = "enable"
	#visionMsg.command = 'set_parameters'	
	#visionMsg.camera_parameters = camParameters
	resp = camera_srv(visionMsg)

	print(resp)

	i = 0
	while not rospy.is_shutdown():
		if i % 10 == 0:
			#print("Depth camera handler is on.")
			pass

		i += 1

if '__name__ == __main__':
	main()
